package ru.am;

import java.util.Scanner;

public class Main {
    public static final String oop = "object-oriented programming";
    public static final String OOP = "OOP";

    public static void main(String[] args) {

        try (Scanner scn = new Scanner(System.in)) {
            String str = scn.nextLine();

            // Блок кода для замены oop на OOP
            int cut = 0;
            for (int i = 0; i < str.length()-oop.length(); i++) {
                if (str.substring(i, i+oop.length()).equalsIgnoreCase(oop)) cut++;
                if (cut%2 == 0 && cut != 0) {
                    str = str.substring(0, i) + OOP + str.substring(i+oop.length());
                    i = i - (oop.length() - OOP.length());
                    cut = 0;
                }
            }
            System.out.println(str);

            String minWord = null; // действующее минимальное слово
            String minWordBuffer = null; // действующее минимальное слово без повторяющихся символов
            boolean latin = true; // находятся ли в слове только латинские буквы?
            int latinWords = 0; // количество слов с латинскими буквами

            System.out.print("Cлова палиндромы: ");
            String word = ""; // текущее слово
            boolean readyWord = false; // слово прочилано полностью?
            for (int i = 0; i < str.length(); i++) { // проходимся по всей строке
                // если текущий символ по таблице ASCII соответствует буквы и цифры, то добавить этот символ к word
                // иначе слово прочитано
                if (str.charAt(i) >= (char) 48 && str.charAt(i) <= (char) 57 ||
                        str.charAt(i) >= (char) 65 && str.charAt(i) <= (char) 90 ||
                        str.charAt(i) >= (char) 97 && str.charAt(i) <= (char) 122 ||
                        str.charAt(i) >= (char) 128) {
                    word = word + str.charAt(i);

                    // если что-то кроме латинских букв, то в слове не только латинские буквы
                    if (!(str.charAt(i) >= (char) 65 && str.charAt(i) <= (char) 90 ||
                            str.charAt(i) >= (char) 97 && str.charAt(i) <= (char) 122)) {
                        latin = false;
                    }
                    // если это последний символ в строке, то слово прочитано полностью
                    if (i == str.length()-1) readyWord = true;
                } else if (!word.isEmpty()) readyWord = true;

                // если прочитанное слово не пустое и прочитано полностью
                if (!word.isEmpty() && readyWord) {
                    // записть в buffer слова без повторяющихся символов
                    String buffer = "";
                    for (int j = 0; j < word.length(); j++) {
                        if (!buffer.contains(Character.toString(word.charAt(j)))) {
                            buffer = buffer + word.charAt(j);
                        }
                    }

                    // если minWord равен null значит это первое вхождение и надо просто записать первое слово
                    // иначе сравниваются размеры действеющего и нового
                    if (minWord != null) {
                        if (buffer.length() < minWordBuffer.length()) {
                            minWord = word;
                            minWordBuffer = buffer;
                        }
                    } else {
                        minWord = word;
                        minWordBuffer = buffer;
                    }

                    // если слово из латинских букв, то увеличить количество
                    if (latin) {
                        latinWords++;
                    }

                    // проверка слова на палиндром
                    boolean palindrome = true;
                    for (int j = 0; j < word.length(); j++) {
                        if (!(word.charAt(j) == word.charAt(word.length() - j - 1))) {
                            palindrome = false;
                            break;
                        }
                    }
                    if (palindrome) { // вывод слово если оно палиндром
                        System.out.print(word+" ");
                    }

                    // обнуление слова и флагов
                    word = "";
                    readyWord = false;
                    latin = true;
                }
            }
            System.out.println("\nПервое слово, в котором число различных символов минимально: " + minWord);
            System.out.println("Количество слов, содержащих только символы латинского алфавита: " + latinWords);

        }
    }
}